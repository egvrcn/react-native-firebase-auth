import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import Header from './components/Header';
import LoginForm from './LoginForm';
import CardSection from './components/CardSection';
import Button from './components/Button';
import Spinner from './components/Spinner';

class Main extends Component {
  state = { loggedIn: null };

  componentWillMount() {
  firebase.initializeApp({
    apiKey: 'AIzaSyDiwneKaiLXxwwPVwKaB0fmHBtD2LkeTOM',
    authDomain: 'rnkimlik.firebaseapp.com',
    databaseURL: 'https://rnkimlik.firebaseio.com',
    projectId: 'rnkimlik',
    storageBucket: 'rnkimlik.appspot.com',
    messagingSenderId: '203686394440'
  });

  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      this.setState({ loggedIn: true });
    } else {
      this.setState({ loggedIn: false });
    }
  });
}

clickLogout() {
  firebase.auth().signOut();
}

renderContent() {
  switch (this.state.loggedIn) {
    case true:
      return (
        <CardSection>
          <Button onPress={this.clickLogout.bind(this)}> ÇIKIŞ YAP </Button>
        </CardSection>
      );
    case false:
    return (
      <LoginForm />
    );
    default:
      return (
        <View>
          <Spinner size="large" />
        </View>
      );

  }
}

  render() {
    return (
      <View>
        <Header headerText="Giriş Ekranı" />
        {this.renderContent()}
      </View>
    );
  }
}

export default Main;
