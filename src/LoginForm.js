import React, { Component } from 'react';
import { TextInput, Alert } from 'react-native';
import firebase from 'firebase';
import Button from './components/Button';
import Card from './components/Card';
import CardSection from './components/CardSection';
import Spinner from './components/Spinner';


class LoginForm extends Component {
  state ={ email: '', password: '', loading: false };

  clickLogin() {
    this.setState({ loading: true });
    const { email, password } = this.state;

    if (email === '' || password === '') {
      this.alertMessager('Hata', 'Email ya da şifreyi doldurunuz!');
      this.setState({ loading: false });
      return;
    }

    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(this.loginSuccess.bind(this))
    .catch(() => {
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(this.loginSuccess.bind(this))
      .catch(this.loginFail.bind(this));
    });
  }

  loginSuccess() {
    console.log('başarılı');
    this.setState({ loading: false });
  }

  loginFail(hata) {
    console.log('hatalı ' + hata);
    this.setState({ loading: false });
    this.alertMessager('HATA', hata+'');
  }

  alertMessager(baslik, icerik) {
    Alert.alert(
      baslik,
      icerik,
      [
        { text: 'Tamam', onPress: () => null }
      ]
    );
  }

  renderButton() {
    if (!this.state.loading) {
      return <Button onPress={this.clickLogin.bind(this)}> GİRİŞ YAP </Button>;
    }
    //loading
    return <Spinner size="small" />
  }

  render() {
    const { inputStyle } = styles;
    return (
      <Card>
        <CardSection>
          <TextInput
            placeholder="E-mail"
            style={inputStyle}
            value={this.state.email}
            onChangeText={emailText => this.setState({ email: emailText })}
           />
        </CardSection>

        <CardSection>
          <TextInput
            secureTextEntry
            placeholder="Şifre"
            style={inputStyle}
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
          />
        </CardSection>

        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card>
    );
  }
}

const styles = {

  inputStyle: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18,
    lineHeight: 23,
    flex: 2
  }
};

export default LoginForm;
